/label ~"type::Bug"

<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
and verify the issue you're about to submit isn't a duplicate.

Please select the correct template above and fill it out below.
These HTML comments will not be rendered so there's no need to delete them.
Do *not* close the issue yourself, we will close things once done/handled accordingly.
For checklists put an x inside the [ ] like this: [x] to mark the checkbox.
The actions at the end of this template will be done automatically once submitted.
--->

# Bug

## Summary

<!-- Summarize the bug encountered concisely. -->

## Environment

- CoolerControl version: `?`
- Installation method: `Flatpak|AppImage|AUR|Source`
- Distribution name and version: `? - i.e. Ubuntu 20.04, Fedora 35, EndeavorOS`
- Desktop: `? - i.e. Gnome, KDE, i3`
- Cooling device model(s): `? - i.e. NZXT Kraken Z63, Corsair Commander Pro`
<!-- add any additional relevant information -->

## Steps to reproduce

<!-- Describe how one can reproduce the issue -->

1. step 1
2. step 2

## Relevant logs and/or screenshots

<!--
Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
 as it's tough to read otherwise.
Logs are very helpful. Run CoolerControl with the `--debug` option to get a lot more output and the logs are then
 also saved under `/tmp/coolercontrol/coolercontrol.log` for easy extraction.
-->

```log output

```

## Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->
